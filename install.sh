#preflight checklist:
#1. install git
#2. install curl
#3. setup SSH key
#4. install with 'curl -Lks goo.gl/y6zzFa | /bin/bash

# maybe I should add automagic installation of vim and git?

git clone --bare https://gitlab.com/bgnielson/dotfiles $HOME/.cfg
function config {
   /usr/bin/git --git-dir=$HOME/.cfg/ --work-tree=$HOME $@
}
mkdir -p .config-backup
config checkout
if [ $? = 0 ]; then
  echo "Checked out config.";
  else
    echo "Backing up pre-existing dot files.";
    config checkout 2>&1 | egrep "\s+\." | awk {'print $1'} | xargs -I{} mv {} .config-backup/{}
fi;
config checkout
config config status.showUntrackedFiles no
